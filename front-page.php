<?php get_header(); ?>

    <div class="w-full lg:h-fphero bg-dkgray relative">
        <div class="md:absolute top-0 right-0 z-0 lg:w-1/2 md:w-1/3 w-full">
            <?php
            $image = get_field('background_image');
            if( !empty( $image ) ): ?>
                <img class="w-full h-fphero object-cover sm:rounded-tl-full md:block" src="<?php echo esc_url($image['url']); ?>" alt="<?php echo esc_attr($image['alt']); ?>" />
            <?php endif; ?>
        </div>

        <div class="container h-full">
            <div class="md:w-1/2 w-full text-white">
                <?php
                $featured_posts = get_field('info');
                if( $featured_posts ): ?>
                    <div class="md:py-[83px] py-12">
                        <?php foreach( $featured_posts as $post ):

                            // Setup this post for WP functions (variable must be named $post).
                            setup_postdata($post); ?>

                            <div class="mt-12">
                                <h2><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
                                <?php the_excerpt(); ?>
                            </div>
                            <div class="mt-12">
                                <a class="px-6 py-4 border border-red rounded-l-full" href="<?php the_permalink(); ?>">Read More</a>
                            </div>

                        <?php endforeach; ?>
                    </div>
                    <?php
                    // Reset the global post object so that the rest of the page works correctly.
                    wp_reset_postdata(); ?>
                <?php endif; ?>
            </div>
        </div>
    </div>

    <section>
        <div class="p-4">
            <div class="flex items-center justify-start">
                <span class="text-h2 text-red flex-initial">Logos</span>
                <span class="border h-0 border-red flex-1 ml-6"></span>
            </div>
            <div class="row js-results">
                <?php
                $loop = new WP_Query(
                    array(
                        'post_type' => 'logos', // This is the name of your post type - change this as required,
                        'posts_per_page' => 4,
                        'order'          => 'ASC',
                        'orderby'        => 'date'
                    )
                );
                while ($loop->have_posts()) : $loop->the_post(); ?>

                    <div class="w-full md:w-1/2 xl:w-1/4 md:my-12 my-6">
                        <?php get_template_part('assets/parts/cards/card-default'); ?>
                    </div>

                <?php endwhile;
                wp_reset_query();
                ?>
            </div>
            <div class="text-center my-12">
                <?php pagination(); ?>
            </div>
        </div>
    </section>

    <section>
        <div class="p-4">
            <div class="flex items-center justify-start">
                <span class="text-h2 text-red flex-initial">Watches</span>
                <span class="border h-0 border-red flex-1 ml-6"></span>
            </div>
            <div class="row js-results">
                <?php
                $loop = new WP_Query(
                    array(
                        'post_type' => 'watches', // This is the name of your post type - change this as required,
                        'posts_per_page' => 4,
                        'order'          => 'ASC',
                        'orderby'        => 'date'
                    )
                );
                while ($loop->have_posts()) : $loop->the_post(); ?>

                    <div class="w-full md:w-1/2 xl:w-1/4 md:my-12 my-6">
                        <?php get_template_part('assets/parts/cards/card-default'); ?>
                    </div>

                <?php endwhile;
                wp_reset_query();
                ?>
            </div>
            <div class="text-center my-12">
                <?php pagination(); ?>
            </div>
        </div>
    </section>

    <section>
        <div class="p-4">
            <div class="flex items-center justify-start">
                <span class="text-h2 text-red flex-initial">Artwork</span>
                <span class="border h-0 border-red flex-1 ml-6"></span>
            </div>
            <div class="row js-results">
                <?php
                $loop = new WP_Query(
                    array(
                        'post_type' => 'artwork', // This is the name of your post type - change this as required,
                        'posts_per_page' => 4, // This is the amount of posts per page you want to show
                        'orderby' => 'rand',
                        'order'    => 'ASC'
                    )
                );
                while ($loop->have_posts()) : $loop->the_post(); ?>

                    <div class="w-full md:w-1/2 xl:w-1/4 md:my-12 my-6">
                        <?php get_template_part('assets/parts/cards/card-default'); ?>
                    </div>

                <?php endwhile;
                wp_reset_query();
                ?>
            </div>

        </div>
    </section>

<?php get_footer();