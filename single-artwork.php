<?php get_header(); ?>

    <div class="absolute w-full top-0 left-0 right-0 bg-gray-200 z-[-1] h-[50vh]">
        <?php WP_Image::display(array('class' => 'object-cover h-[50vh] w-full')); ?>
    </div>

    <section class="mt-[400px] min-h-[600px]">
        <div class="container">
            <article class="relative">
                <div class="lg:w-1/3 w-full lg:absolute top-[-3%] left-0 z-10 mb-4 bg-grey-200">
                    <?php WP_Image::display(array('class' => 'w-full object-cover border-2 border-white shadow-lg rounded-xl ')); ?>
                    <div class="mx-auto lg:w-1/2 w-2/3">
                        <div class="px-3 py-2 mb-3">
                            <h4 class="m-0">Program:</h4>
                            <?php
                            $program = get_field('program');
                            if ($program): ?>
                                <?= $program ?>
                            <?php else: ?>
                                <p>N/A</p>
                            <?php endif; ?>
                        </div>
                        <div class="px-3 py-2 mb-3">
                            <h4 class="m-0">Dimensions:</h4>
                            <?php
                            $dimentions = get_field('original_dimentions');
                            if ($dimentions): ?>
                                <?= $dimentions ?>
                            <?php else: ?>
                                <p>N/A</p>
                            <?php endif; ?>
                        </div>
                        <div class="px-3 py-2 mb-3">
                            <h4 class="m-0">File Type:</h4>
                            <?php
                            $file = get_field('file_type');
                            if ($file): ?>
                                <?= $file ?>
                            <?php else: ?>
                                <p>N/A</p>
                            <?php endif; ?>
                        </div>
                        <div class="px-3 py-2 mb-3">
                            <?php
                            $link = get_field('download');
                            if( $link ):
                                $link_url = $link['url'];
                                $link_title = $link['title'];
                                $link_target = $link['target'] ? $link['target'] : '_self';
                                ?>
                                <a class="w-1/2 my-4 px-12 py-4 rounded-full bg-red text-center text-white mx-auto"
                                   href="<?= $link_url ?>" target="<?= $link_target ?>"><?= $link_title ?></a>
                            <?php else: ?>
                                <p>N/A</p>
                            <?php endif; ?>
                        </div>
                    </div>
                </div>
                <div class="flex py-4 mt-24">
                    <div class="w-1/3 lg:block hidden"></div>
                    <div class="lg:w-2/3 w-full border bg-white rounded-xl shadow-lg lg:ml-minussix m-0 px-6">
                        <h1 class="text-center break-words text-red uppercase my-12"><?php the_title(); ?></h1>
                        <?php the_content(); ?>
                    </div>
                </div>
            </article>
        </div>
    </section>

<?php get_footer(); ?>