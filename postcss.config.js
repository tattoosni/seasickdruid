module.exports = {
    plugins:
        process.env.NODE_ENV === 'production'
            ? {
                'postcss-import': {},
                tailwindcss: {},
                'postcss-nested': {},
                'autoprefixer': {},
                'cssnano' : {
                    preset: [ "default", { discardComments: { removeAll: true} }]
                },
                'postcss-hash': {
                    //algorithm: 'sha256',
                    trim: 20,
                    manifest: './assets/build/manifest.json'
                }
            }
            : {
                // development tasks
                'postcss-import': {},
                tailwindcss: {},
                'postcss-nested': {},
                'autoprefixer': {},
            },
}