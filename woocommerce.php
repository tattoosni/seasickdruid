<?php get_header(); ?>

 <section class="my-32">

        <div class="container">
            <div class="row justify-center">
                <div class="w-full md:w-2/3 mb-12">
                    <div class="">
                        <div class="aspect-h-9 aspect-w-16 mb-4 bg-grey-200">
//                            <?php WP_Image::display(array('class' => 'object-cover')); ?>
                        </div>
                        <h1><?php the_title(); ?></h1>
                        <div class="content-wrapper"><?php woocommerce_content(); ?></div>
                    </div>
                </div>
            </div>

        </div>

    </section>

<?php get_footer(); ?>