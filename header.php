<!DOCTYPE html>
<html lang="en">
<head>
    <script src="https://use.fontawesome.com/80b0595e3d.js"></script>
    <?php wp_head(); ?>

    <?php do_action('favicon_head'); ?>
    <meta name="google-site-verification" content="9f0M7-oCbWhp0ZLfb8BIu5cag4Isfoh-5l-dspId_-c" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="theme-color" content="<?= THEME_COLOUR; ?>">

    <?php
    if (isset($_COOKIE['cookieconsent_status'])):
        if (G_TAG): ?>
            <!-- Google Analytics -->
            <script async src="https://www.googletagmanager.com/gtag/js?id=<?= G_TAG; ?>"></script>
            <script>window.dataLayer = window.dataLayer || [];

                function gtag() {
                    dataLayer.push(arguments);
                }

                gtag('js', new Date());
                gtag('config', '<?= G_TAG; ?>');</script>
        <?php endif;
    endif; ?>
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-205016034-1">
    </script>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());

        gtag('config', 'UA-205016034-1');
    </script>
</head>
<body <?php body_class(); ?>>

<div class="header-message bg-dkgray border-b border-red h-auto w-full text-white">
    <div class="container">
        <div class="row align-center items-center py-2 justify-center">
            <?php
                $link = get_field('header_link', 'option');
                if( $link ):
                    $link_url = $link['url'];
                    $link_title = $link['title'];
                    $link_target = $link['target'] ? $link['target'] : '_blank';
            ?>
            <span class="text-h4"><?php the_field('header_text', 'option'); ?></span>
            <span class=""><a class="text-h4 underline" href="<?php echo esc_url($link_url); ?>" target="<?php echo esc_attr($link_target); ?>"><?php echo esc_html($link_title); ?></a></span>
            <span class="message-hide">
                <svg xmlns="http://www.w3.org/2000/svg" width="14" height="14" viewBox="0 0 14 14">
                    <path id="Path_1693" data-name="Path 1693" d="M19,6.41,17.59,5,12,10.59,6.41,5,5,6.41,10.59,12,5,17.59,6.41,19,12,13.41,17.59,19,19,17.59,13.41,12Z" transform="translate(-5 -5)" fill="#fff"/>
                </svg>
            </span>
            <?php endif; ?>
        </div>
    </div>
</div>

<header class="py-2 w-full border-b-2 border-red bg-black text-white <?php if (is_user_logged_in()): ?>top-[32px]<?php endif; ?>">

    <div class="container">

        <div class="flex items-center">

            <div class="md:w-1/4 w-1/2 flex items-center">
                <a href="<?php bloginfo('url'); ?>">
                    <?php
                    $custom_logo_id = get_theme_mod( 'custom_logo' );
                    $logo = wp_get_attachment_image_src( $custom_logo_id , 'full' );

                    if ( has_custom_logo() ) {
                      echo '<img src="' . esc_url( $logo[0] ) . '" alt="' . get_bloginfo( 'name' ) . '">';
                    } else {
                    ?>
                    <div class="text-h4"><?php bloginfo('name'); ?></div>
                    <?php
                        }
                    ?>
                </a>
            </div>

            <div class="md:w-3/4 w-1/2 flex item-center justify-end">

                <div class="mobile-toggle md:hidden h-8 w-8 cursor-pointer my-auto text-black relative z-10">
                    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512" class="w-full h-full">
                        <path fill="currentColor"
                              d="M16 132h416c8.837 0 16-7.163 16-16V76c0-8.837-7.163-16-16-16H16C7.163 60 0 67.163 0 76v40c0 8.837 7.163 16 16 16zm0 160h416c8.837 0 16-7.163 16-16v-40c0-8.837-7.163-16-16-16H16c-8.837 0-16 7.163-16 16v40c0 8.837 7.163 16 16 16zm0 160h416c8.837 0 16-7.163 16-16v-40c0-8.837-7.163-16-16-16H16c-8.837 0-16 7.163-16 16v40c0 8.837 7.163 16 16 16z"
                              class=""></path>
                    </svg>
                </div>

                <nav class="w-full h-full hidden md:block">
                    <?php wp_nav_menu(array(
                        'theme_location' => 'header_menu',
                        'menu_class' => 'main-menu flex flex-col items-center space-y-4 md:space-y-0 md:space-x-4 md:flex-row md:justify-end',
                        'walker' => new Custom_Menu_Walker()
                    )); ?>
                </nav>
                <span class="ml-12">
                    <form class="" method="get" action="">
                        <div class="flex items-center">
                            <input type="text" class="p-2 h-8 rounded-l-full" name="s" id="s"  placeholder="Search" />
                            <button class="px-3 py-1 border border-red">
                               <i class="fa fa-search text-red" aria-hidden="true"></i>
                            </button>
                        </div>
                    </form>
                </span>

            </div>

        </div>

    </div>

</header>
<main class="mb-32 overflow-x-hidden min-h-[50vh]">
