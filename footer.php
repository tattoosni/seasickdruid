</main>
<footer class="bg-dkgray text-white flex justify-end items-end py-12">
    <div class="container">

        <div class="flex flex-wrap justify-between">
            <div class="md:w-1/2 w-full mb-12 footer-form">
                <div>
                    <h3>Newsletter signup</h3>
                    <p>Subscribe to our email newsletter for the latest news and events straight to your inbox</p>
                    <?php echo do_shortcode('[gravityform id="2" title="false" description="false"]'); ?>
                </div>
            </div>

            <div class="md:w-1/3 w-full ">
                <?php if(have_rows('footer_links', 'options')):
                    while(have_rows('footer_links', 'options')): the_row();
                ?>
                <div class="flex">
                    <div class="md:w-1/2 w-full mb-12">

                        <div class="js-footer-toggle flex items-center justify-between mb-2">
                            <span class="block font-heading text-h4 font-bold">Socials</span>
                            <button class="md:hidden">
                                <span class="transform rotate-180 transition-transform duration-200">+</span>
                            </button>
                        </div>

                        <div class="hidden md:block">
                            <?php if(have_rows('page_links')):?>
                            <ul>
                                <?php while(have_rows('page_links')): the_row();
                                $link = get_sub_field('link');
                                if( $link ):
                                    $link_url = $link['url'];
                                    $link_title = $link['title'];
                                    $link_target = $link['target'] ? $link['target'] : '_blank';
                                ?>
                                    <li class="py-2 m-0"> <a class="button" href="<?= ( $link_url ); ?>" target="<?= ( $link_target ); ?>"><?= ( $link_title ); ?></a></li>
                                <?php endif; endwhile;?>
                            </ul>
                            <?php endif; ?>
                        </div>

                    </div>

                    <div class="md:w-1/2 w-full mb-12">

                        <div class="js-footer-toggle flex items-center justify-between mb-2">
                            <span class="block font-heading text-h4 font-bold">Legal</span>
                            <button class="md:hidden">
                                <span class="transform rotate-180 transition-transform duration-200">+</span>
                            </button>
                        </div>

                        <div class="hidden md:block">
                            <?php if(have_rows('legal_links')):?>
                                <ul>
                                    <?php while(have_rows('legal_links')): the_row();
                                    $link = get_sub_field('link');
                                        if( $link ):
                                            $link_url = $link['url'];
                                            $link_title = $link['title'];
                                            $link_target = $link['target'] ? $link['target'] : '_blank';
                                    ?>
                                        <li class="py-2 m-0"> <a class="button" href="<?= ( $link_url ); ?>" target="<?= ( $link_target ); ?>"><?= ( $link_title ); ?></a></li>
                                    <?php endif; endwhile;?>
                                </ul>
                            <?php endif; ?>
                        </div>

                    </div>
                </div>
                <div class="w-full min-h-[50px] flex justify-start items-center">
                    <?php if(have_rows('social_links')):?>
                        <?php while(have_rows('social_links')): the_row();
                            $svg = get_sub_field('icon');
                        ?>
                            <span class="py-2 mx-2"><a class="button" href="<?= ( $link_url ); ?>" target="<?= ( $link_target ); ?>"><?= ($svg); ?></a></span>
                            <?php endwhile;?>
                    <?php endif; ?>
                </div>
                <?php endwhile; endif;?>
            </div>

        </div>

        <div class='text-center md:text-right mt-10'>
            <span>Designed and developed by </span>
            <a class="text-h4 uppercase font-bold" target='_blank' rel="noreferrer" href="https://www.seasickdruid.co.uk/">Seasickdruid </a>
            <span> © <?php echo Date('Y') ?></span>
        </div>
    </div>
</footer>
<?php wp_footer(); ?>
</body>
</html>
