<?php get_header();

if (have_posts()) : ?>

<section class="my-32">

    <div class="p-4">
        <div class="row js-results">
            <?php while (have_posts()) : the_post(); ?>

                <div class="w-full md:w-1/4 mb-12">
                    <?php get_template_part('assets/parts/cards/card-default'); ?>
                </div>

            <?php endwhile;?>
        </div>

        
        <div class="text-center my-12">
            <?php pagination(); ?>
        </div>
    </div>

</section>


<?php else:

    //if no page exists -> 404 and show this message
    if(is_404()):
        get_template_part('assets/parts/404');

    endif; //end 404
    
endif;
get_footer(); ?>