<?php 

//get randomised main.css filename
function get_revision_filename($filename)
{
    $file = get_theme_file_path('/assets/build/manifest.json');

    if (file_exists($file)) {
        $manifest = file_get_contents($file);
        if ($manifest === false) return $filename;

        $paths = json_decode($manifest);
        return $paths->{$filename};
    }
    return $filename;
}

//Setup frontend assets
add_action('wp_enqueue_scripts', function(){

    wp_enqueue_script( 'jquery' );
    wp_enqueue_style('main-style', get_template_directory_uri() . '/assets/build/' . get_revision_filename('frontend.css'));
    wp_enqueue_style('custom-google-fonts', 'https://fonts.googleapis.com/css2?family=Nunito:wght@400;700;800&display=swap', false);
    wp_enqueue_script('lozad', 'https://cdn.jsdelivr.net/npm/lozad/dist/lozad.min.js', array(), '1.0.0', true);

    if(isset($_COOKIE['cookieconsent_status']) === false):
        wp_enqueue_style('cookieconsent-style', 'https://cdn.jsdelivr.net/npm/cookieconsent@3.1.1/build/cookieconsent.min.css');
        wp_enqueue_script('cookieconsent', 'https://cdn.jsdelivr.net/npm/cookieconsent@3.1.1/build/cookieconsent.min.js', array(), '3.1.1', true);
    endif;

    //if(!is_front_page() && (is_page() || is_archive())) {
    //    wp_enqueue_script('swiper', 'https://cdnjs.cloudflare.com/ajax/libs/Swiper/4.5.1/js/swiper.min.js', array(), '4.5.1', true);
    //    wp_enqueue_style('swiper-style', 'https://cdnjs.cloudflare.com/ajax/libs/Swiper/4.5.1/css/swiper.min.css', false);
    //}

});


//Favicon
function add_favicon() {
    $favicon_url = get_stylesheet_directory_uri() . '/assets/img/favicon-full-black.png';
    echo '<link rel="icon" type="image/png" href="' . $favicon_url . '"  sizes="128x128" />';
}
add_action('login_head', 'add_favicon');
add_action('admin_head', 'add_favicon');
add_action('favicon_head', 'add_favicon');


//Editor styles
add_action('after_setup_theme', function() {

    //required for following functions to work
    add_theme_support( 'editor-styles' );

    //load custom admin styles
    add_editor_style('assets/build/backend.css');

    //load fonts for admin
    add_editor_style( 'https://fonts.googleapis.com/css?family=Open+Sans:400,700,900&display=swap' );

});


//Inline JS in footer
add_action('wp_footer', 'add_inline_scripts');
function add_inline_scripts() {
    if (file_exists(get_theme_file_path("/assets/build/build.js"))) {
        echo '<script id="theme-build-js">';
        include(get_theme_file_path("/assets/build/build.js"));
        echo '</script>';
    }
    else if(file_exists(get_theme_file_path("/assets/js/custom.js"))) {
        echo '<script id="theme-custom-js">';
        include(get_theme_file_path("/assets/js/custom.js"));
        echo '</script>';
    }

    if(isset($_COOKIE['cookieconsent_status']) === false):
        if (file_exists(get_theme_file_path("/assets/js/custom/cookie-consent.js"))) {
            echo '<script>';
            include(get_theme_file_path("/assets/js/custom/cookie-consent.js"));
            echo '</script>';
        }
    endif;

    //for dev, to hide wp menu on mobile
    if(is_user_logged_in()):
        echo "<style>@media (max-width: 782px) {html #wpadminbar { display:none;} html {margin:0 !important;}}</style>";
    endif;
}


//add defer to scripts
function add_async_attribute($tag, $handle) {

    $tag = str_replace("type='text/javascript' ", "", $tag);

    if (is_admin() || 'jquery-core' || 'jquery' || 'jquery-js' === $handle) {
        return $tag;
    }
    return str_replace(' src', ' defer src', $tag);
}
add_filter('script_loader_tag', 'add_async_attribute', 10, 2);


//remove version query string on files
function _remove_script_version($src) {
    $parts = explode('?ver', $src);
    return $parts[0];
}
add_filter('script_loader_src', '_remove_script_version', 15, 1);
add_filter('style_loader_src', '_remove_script_version', 15, 1);


//remove jquery migrate
function remove_jquery_migrate($scripts) {
    if (!is_admin() && isset($scripts->registered['jquery'])) {
        $script = $scripts->registered['jquery'];
        if ($script->deps) {
            $script->deps = array_diff($script->deps, array('jquery-migrate'));
        }
    }
}
add_action('wp_default_scripts', 'remove_jquery_migrate');


//remove some stuff from the <head>
add_action( 'init', function () {
    remove_action('wp_head', 'print_emoji_detection_script', 7);
    remove_action('admin_print_scripts', 'print_emoji_detection_script');
    remove_action('wp_print_styles', 'print_emoji_styles');
    remove_action('admin_print_styles', 'print_emoji_styles');
    remove_action( 'wp_head', 'rsd_link' );
    remove_action( 'wp_head', 'wlwmanifest_link' );
    remove_action( 'wp_head', 'wp_generator' );
    remove_action( 'template_redirect', 'rest_output_link_header', 11 );
    remove_action( 'wp_head', 'wp_oembed_add_discovery_links' );
    remove_action( 'wp_head', 'rest_output_link_wp_head' );
} );


//removing comments
function action_wp_before_admin_bar_render() {
    global $wp_admin_bar;
    $wp_admin_bar->remove_menu( 'comments' );
}

function action_admin_menu() {
    remove_menu_page( 'edit-comments.php' );
}

function action_init() {
    remove_post_type_support( 'post', 'comments' );
    remove_post_type_support( 'page', 'comments' );
}

add_action( 'wp_before_admin_bar_render', 'action_wp_before_admin_bar_render' );
add_action( 'admin_menu', 'action_admin_menu' );
add_action( 'init', 'action_init' );
add_filter( 'feed_links_show_comments_feed', '__return_false' );


function dequeue_scripts() {
    wp_deregister_script( 'wp-embed' );
}
add_action( 'wp_enqueue_scripts', 'dequeue_scripts' );