<?php


/**
 * Class WP_Image
 */
class WP_Image {

    /**
     * Display featured image or media item with fallback support
     * @param array $args
     * @return bool
     */
    public static function display(array $args = []): bool {

        $defaults = array(
            'ID' => null,
            'size' => 'large',
            'class' => 'object-cover h-full w-full',
            'fallback' => true,
        );

        $parsed_args = wp_parse_args( $args, $defaults );

        if($parsed_args['ID'] === null) {
            global $post;
            $image_id = get_post_thumbnail_id($post);
        }
        else {
            $image_id = $parsed_args['ID'];
        }

        if($image_id):
            echo wp_get_attachment_image($image_id, $parsed_args['size'], false, array(
                'class' => $parsed_args['class']
            ));

        elseif($parsed_args['fallback']):
            $fallback_svg = "<span class='w-1/2 h-1/2 opacity-25'>" .get_svg('fallback') . "</span>";
            echo "<div class='bg-gray-200 flex items-center justify-center {$parsed_args['class']}'>{$fallback_svg}</div>";
        endif;

        return false;

    }

}

/**
 * Returns the contents of a .svg file, which can be modified or displayed.
 * @param $filename
 * @return false|string
 */
function get_svg($filename) {
    $magic_path = 'assets/images/svg/' . $filename  . ".svg";
    if (file_exists(get_theme_file_path($magic_path))) {
        return file_get_contents(get_theme_file_path($magic_path));
    }
}
