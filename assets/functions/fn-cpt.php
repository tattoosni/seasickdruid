<?php

/**
 * Class RegisterCPT
 */
class RegisterCPT {

    /**
     * Generate Taxonomy
     * @param string $tax_title
     * @param string $tax_slug
     * @param array|string $post_type
     */
    function tax(string $tax_title, string $tax_slug, $post_type) {

        register_taxonomy(
            $tax_slug,
            $post_type,
            array(
                'label' => $tax_title,
                //'rewrite' => array( 'slug' => 'type' ),
                'hierarchical' => true,
                'show_in_rest' => true

            )
        );

    }

    /**
     * Generate Custom Post Type
     * @param string $name
     * @param string $singular_name
     * @param string $slug
     * @param array $custom
     */
    function cpt(string $name, string $singular_name, string $slug, array $custom = array()) {
        
        static $count = 0;
        $count++;

        $single = $singular_name === false ? $name : $singular_name;
        $labels = array(
            'name'               => _x( $name, 'post type general name' ),
            'singular_name'      => _x( $single, 'post type singular name' ),
            'add_new'            => _x( 'Add New', 'add new' ),
            'add_new_item'       => __( 'Add New ' . $single ),
            'edit_item'          => __( 'Edit ' . $single ),
            'new_item'           => __( 'New ' . $single ),
            'all_items'          => __( 'All ' . $name ),
            'view_item'          => __( 'View ' . $single ),
            'search_items'       => __( 'Search ' . $name ),
            'not_found'          => __( 'No ' .$name.  ' found' ),
            'not_found_in_trash' => __( 'No ' .$name. ' found in the Trash' ),
            'parent_item_colon'  => '',
            'menu_name'          => $name
        );
        $args = array(
            'labels'        => $labels,
            'description'   => 'A custom post type',
            'public'        => true,
            'menu_position' => isset($custom['position']) ? $custom['position'] : 5 + ($count * 0.001),
            'supports'      => array('title', 'editor', 'thumbnail', 'excerpt', 'page-attributes'),
            'has_archive'   => isset($custom['archive']) ? $custom['archive'] : true,
            'show_in_rest'  => true,
            'hierarchical'  => true,
            'rewrite'       => true,
            'capability_type' => 'post',
            'query_var' => isset($custom['single']) ? $custom['single'] : true,
            'publicly_queryable' => isset($custom['public']) ? $custom['public'] : true,
        );
        register_post_type($slug, $args);
    }
}


add_action( 'init', function() {

    //setup
    $register = new RegisterCPT();

    //declare your custom post types and taxonomies in this format

    $register->cpt('Watches', 'Watch', 'watches');
    $register->cpt('Artwork', 'Artwork', 'artwork');
    $register->cpt('Logos', 'Logos', 'logos');
    $register->cpt('Snippets', 'Snippets', 'snippets');
    $register->tax('Watches Category', 'watches-category', 'watches');
    $register->tax('Artwork Category', 'artwork-category', 'artwork');
    $register->tax('Logos Category', 'logos-category', 'logos');
    $register->tax('Snippets Category', 'snippets-category', 'snippets');

    //$register->cpt('Services', 'Service', 'services');
    //$register->tax('Service Category', 'service-category', 'services');

});


/**
 * Example pre_get_posts function
 * @param $query
 */
//add_action( 'pre_get_posts', 'show_all_cpt_items' );
function show_all_cpt_items($query) {

    if ( is_admin() || ! $query->is_main_query() ) return;

    if ( in_array ( $query->get('post_type'), array('services','blog') ) ) {
        $query->set( 'posts_per_page', -1 );
        return;
    }
}
