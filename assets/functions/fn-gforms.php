<?php

//force ajax
add_filter('gform_form_args', function($args){
    $args['ajax'] = true;
    return $args;
}, 10, 1);


//custom classes for submit button
function gform_submit_button_custom_class( $button, $form ) {
    $dom = new DOMDocument();
    $dom->loadHTML( '<?xml encoding="utf-8" ?>' . $button );
    $input = $dom->getElementsByTagName( 'input' )->item(0);

    //set custom classes for the submit button
    $classes = " btn";

    $input->setAttribute( 'class', $classes );
    return $dom->saveHtml( $input );
}
add_filter( 'gform_submit_button', 'gform_submit_button_custom_class', 10, 2 );

//custom classes for inputs
function custom_gform_field_class( $classes, $field, $form ) {

    $fields = ['text', 'email', 'phone'];

    if ( in_array($field->type, $fields) ) {
        $classes .= ' border-2 border-white block w-full px-3 py-2 outline-none';
   }
    elseif($field->type == 'fileupload') {
        $classes .= ' text-black font-semibold';
    }
    return $classes;
}
//add_filter( 'gform_field_css_class', 'custom_gform_field_class', 10, 3 );
