<?php

//Add ACF to Admin sidebar
if( function_exists('acf_add_options_page') ) {

    // Add parent.
    $parent = acf_add_options_page(array(
        'page_title'  => __('Theme Settings'),
        'menu_title'  => __('Theme Settings'),
        'capability' => 'publish_pages', //set this to 'manage_options' to be admin only
        'redirect'    => true, //redirect to child page
        'position' => '1.01', //add to top
    ));

    //sub page
    $child = acf_add_options_sub_page(array(
        'page_title'  => __('Header Message'),
        'menu_title'  => __('Header Message'),
        'parent_slug' => $parent['menu_slug'],
    ));

    //sub page 2
    $child = acf_add_options_sub_page(array(
        'page_title'  => __('Footer Links'),
        'menu_title'  => __('Footer Links'),
        'parent_slug' => $parent['menu_slug'],
    ));

}


/**
 * Declares custom block category
 *
 */
add_filter( 'block_categories', function( $categories, $post ) {
    return array_merge(
        $categories,
        array(
            array(
                'slug' => 'custom-blocks',
                'title' => __( 'Custom Blocks', 'custom-blocks' ),
                'icon'  => 'hammer',
            ),
        )
    );
}, 10, 2 );


//declare blocks
add_action('acf/init', 'wibble_acf_block_init');
function wibble_acf_block_init() {
    if( function_exists('acf_register_block_type') ) {

        acf_register_block_type(array(
            'name'				=> 'accordion',
            'title'				=> __('Accordion'),
            'description'		=> __('This is an Accordion'),
            'icon'              => 'hammer',
            'render_callback'	=> 'acf_block_callback',
            'category'			=> 'custom-blocks',
            'post_types'        => array('snippets'),
            'mode'              => 'edit',
            'supports'          => array(
                'mode' => false,
            ),
        ));
        acf_register_block_type(array(
            'name' => 'cards',
            'title' => __('Posts Cards'),
            'description' => __('Select your posts for featured products'),
            'render_callback' => 'acf_block_callback',
            'category' => 'common',
        ));
        acf_register_block_type(array(
            'name' => 'flip-cards',
            'title' => __('Flip Cards'),
            'description' => __('Flip Cards Block'),
            'render_callback' => 'acf_block_callback',
            'category' => 'common',
        ));
        acf_register_block_type(array(
            'name' => 'spider-section',
            'title' => __('Spider Section'),
            'description' => __('Spider Image Block'),
            'render_callback' => 'acf_block_callback',
            'category' => 'common',
        ));
    }
}
function acf_block_callback( $block){
    $slug = str_replace('acf/', '', $block['name']);
    if (file_exists(get_theme_file_path("/assets/blocks/block-{$slug}.php"))) {
        include(get_theme_file_path("/assets/blocks/block-{$slug}.php"));
    }
}


/*Change location for acf-json folder - SAVING */
add_filter('acf/settings/save_json', 'my_acf_json_save_point');
function my_acf_json_save_point( $path ) {
    $path = get_stylesheet_directory() . '/assets/acf-json/';
    return $path;
}


/*Change location for acf-json folder - LOADING */
add_filter('acf/settings/load_json', 'my_acf_json_load_point');
function my_acf_json_load_point($paths) {
    unset($paths[0]);
    $paths[] = get_stylesheet_directory() . '/assets/acf-json/';
    return $paths;
}
