<?php 

//Custom Pagination
function pagination() {
    echo "<div class='js-pagination'>";
        echo get_the_posts_pagination(array(
            'prev_text' => 'Back',
            'next_text' => 'Next',
            'class' => 'hidden'
        ));
    echo "</div>";

    echo "<button class='js-load-more'>Load More</button>";
}


class Custom_Menu_Walker extends Walker_Nav_Menu {
    function start_lvl(&$output, $depth = 0, $args = NULL){
        $indent = str_repeat("\t", $depth);
        $output .= "\n$indent<ul class=\"submenu z-10 hidden relative md:absolute p-6 bg-white uppercase text-sm rounded shadow space-y-2 \">\n";
    }

    function start_el( &$output, $item, $depth = 0, $args = null, $id = 0 ) {

        $object = $item->object;
    	$type = $item->type;
    	$title = $item->title;
    	$description = $item->description;
    	$permalink = $item->url;
    	$classes = $item->classes;
    	if($classes) {
    	    $classes = implode(" ", $item->classes);
        }

      $output .= "<li class='{$classes}'>";
        
      //Add SPAN if no Permalink
      if( $permalink && $permalink != '#' ) {
      	$output .= '<a href="' . $permalink . '">';
      } else {
      	$output .= '<span>';
      }
       
      $output .= $title;

      if( $description != '' && $depth == 0 ) {
      	$output .= '<small class="description screen-reader-text">' . $description . '</small>';
      }

      if( $permalink && $permalink != '#' ) {
      	$output .= '</a>';
      } else {
      	$output .= '</span>';
      }
    }
}


register_nav_menu('header_menu', __('Header Menu'));

function themename_custom_logo_setup() {
    $defaults = array(
        'height'               => 100,
        'width'                => 100,
        'flex-height'          => true,
        'flex-width'           => true,
        'header-text'          => array( 'site-title', 'site-description' ),
        'unlink-homepage-logo' => true,
    );

    add_theme_support( 'custom-logo', $defaults );
}

add_action( 'after_setup_theme', 'themename_custom_logo_setup' );