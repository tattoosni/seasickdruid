<?php
if (have_rows('images')) : ?>
    <div class="container mb-12">
        <div class="xl:flex w-full lg:justify-between">
        <?php while(have_rows('images') ) : the_row();
            $bg_image = get_sub_field('bg_image');
            $main_image = get_sub_field('main_image');
            $fg_image = get_sub_field('fg_image');
        ?>
            <div class="relative min-h-[320px] w-1/3 mr-12 mb-0">
                <div class="w-full absolute z-0 w-[320px] h-[320px] top-0 left-0">
                    <?= $bg_image ?>
                </div>

                <div class="absolute z-10 w-[220px] h-[220px] top-[50px] left-[50px] rounded-xl shadow-lg overflow-hidden"><?php
                $main_image = get_sub_field('main_image');
                if( !empty( $main_image ) ): ?>
                    <img class="bg-gray-200 w-[220px] h-auto object-cover" src="<?php echo esc_url($main_image['url']); ?>" alt="<?php echo esc_attr($main_image['alt']); ?>" />
                <?php endif; ?></div>

                <div class="w-full absolute z-10 w-[320px] h-[320px] top-0 left-[15px]">
                    <?= $fg_image ?>
                </div>
            </div>
        <?php endwhile; ?>
            <div class="w-full xl:w-2/3 lg:1/2 py-8 p-4 -ml-1 mt-0">
                <h3 class="pl-3"><?= get_field('spider_info_h3'); ?></h3>
                <div class="pl-3"><?= get_field('spider_info'); ?></div>
            </div>
        </div>
    </div>
    <?php wp_reset_postdata(); ?>
<?php endif; ?>
