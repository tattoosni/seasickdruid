<?php if (have_rows('accordion_repeater')) : ?>
    <div class="row">
        <?php

        $counter = -1;

        while (have_rows('accordion_repeater')) : the_row();
            $counter++;
            $title = get_sub_field('accordion_header');
            $svg = get_sub_field('accordion_svg');
            $content = get_sub_field('accordion_content');
            ?>
            <div class="mb-12 xl:w-1/4 md:w-1/2 w-full">
                <input type="radio" name="tailwind_accordion" id="tab<?= $counter; ?>"
                       class="peer px-2 py-4 hidden">
                <label for="tab<?= $counter; ?>"
                       class="bg-red py-12 peer-checked:min-h-[50px] peer-checked:hidden rounded-lg text-center peer-checked:rounded-b-none overflow-hidden p-4 block text-white font-bold cursor-pointer hover:bg-dkred">
                    <div class="w-1/3  mx-auto"><?= $svg ?></div>
                    <?= $title ?>
                </label>
                <label for="tab<?= $counter; ?>"
                       class="bg-red peer-checked:block hidden rounded-lg text-center peer-checked:rounded-b-none overflow-hidden p-4 text-white font-bold cursor-pointer relative hover:bg-dkred">
                    <?= $title ?>
                </label>
                <div class="peer-checked:block bg-white leading-relaxed p-4 hidden">
                    <p class="text-lg font-bold py-4"><?= $title ?></p>
                    <p><?= $content ?></p>
                </div>

            </div>
        <?php endwhile; ?>
    </div>
<?php endif; ?>

