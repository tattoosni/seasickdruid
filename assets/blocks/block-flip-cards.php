<?php if(have_rows('flip_card')) : ?>
    <div class="card-container row">
        <?php while(have_rows('flip_card')) : the_row();
            $fronttitle = get_sub_field('front_title');
            $backtitle = get_sub_field('back_title');
            ?>
            <div class="cards">
                <div class="back">
                    <h3><?= $backtitle ?></h3>
                    <?php if( have_rows('back_info')) : while(have_rows('back_info')) : the_row();
                        $listitem = get_sub_field('list_item');
                        ?>
                        <p><?= $listitem ?></p>
                    <?php endwhile;
                    else:
                        //do nothing
                    endif; ?>
                </div>
                <div class="front">
                    <h2><?= $fronttitle ?></h2>
                </div>
            </div>
        <?php endwhile; ?>
    </div>
<?php endif; ?>
