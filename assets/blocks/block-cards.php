<?php
global $post;
$c_posts = get_field('c_post');
if ($c_posts): ?>
    <div class="md:flex flex-wrap -mx-2">
        <?php foreach ($c_posts as $post):
            setup_postdata($post); ?>
            <div class="lg:w-1/4 md:w-1/2 w-full p-2">
                <div class="bg-gray-200 rounded-lg overflow-hidden">
                    <div class="p-2">
                        <h4 class="min-h-[50px]"><?php the_title(); ?></h4>
                    </div>
                    <div>
                        <img class="h-[200px] w-full object-cover" src="<?= get_the_post_thumbnail_url($post->ID); ?>">
                    </div>
                    <div class="mt-0 w-full bg-gray-200 py-6 text-center">
                        <a class="font-bold !no-underline" href="<?php the_permalink($post->ID); ?>">Learn More</a>
                    </div>
                </div>
            </div>
        <?php endforeach; ?>
    </div>
    <?php wp_reset_postdata(); ?>
<?php endif; ?>
