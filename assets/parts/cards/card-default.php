<article class="shadow-lg h-full rounded-xl overflow-hidden">
    <div class="relative">
        <a class="" href="<?php the_permalink(); ?>">
            <?php if (has_post_thumbnail()) {
                WP_Image::display(array('class' => 'object-cover xl:h-[500px] lg:h-[350px] h-[250px] w-full filter grayscale hover:grayscale-0 rounded-xl shadow-lg'));
            } else { ?>
                <img class="object-cover xl:h-[500px] h-[250px] w-full filter grayscale hover:grayscale-0 rounded-xl shadow-lg" src="/wp-content/uploads/woocommerce-placeholder.png" >
            <?php } ?>
        <div class="absolute hover:text-red font-bold text-white px-2 top-1/2 left-0 z-10 text-center w-full"><?php the_title(); ?></div></a>
    </div>
</article>