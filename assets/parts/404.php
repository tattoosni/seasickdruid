<section>
    <div class="container">
        <h1>404</h1>
        <p>There's been an error. We couldn't find the page you were looking for.</p>
    </div>
</section>