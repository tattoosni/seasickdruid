<section>
    <div class="container">
        <div class="row">
            <div class="w-full md:w-1/2">
                <h2>Pages</h2>
                <ul><?php wp_list_pages(array('title_li' => '')); ?></ul>
            </div>
            <div class="w-full md:w-1/2">
                <h2>News</h2>
                <?php
                $blog = new WP_Query( array('post_type' => 'post', 'posts_per_page'=> -1) );
                if( $blog->have_posts() ) :
                    echo '<ul>';
                    while( $blog->have_posts() ) : $blog->the_post();
                        $title = get_the_title();
                        $url = get_the_permalink();
                        echo "<li><a href='{$url}'>{$title}</a></li>";
                    endwhile;
                    echo '</ul>';
                endif; ?>
            </div>
        </div>
    </div>
</section>