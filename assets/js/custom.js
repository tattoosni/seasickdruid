jQuery(document).ready(function ($) {

    /**
     * Desktop and mobile sub menu handling
     *
     *
     *
     */

    //header > nav
    var nav = document.querySelector("header nav");

    //li (that has dropdown class)
    var children = nav.querySelectorAll(".menu-item.menu-item-has-children");
    var submenu = document.querySelectorAll(".submenu");
    var sub_submenu = document.querySelectorAll(".submenu .submenu");

    var active_sub_class = "v--show";
    var active_class = "active";
    var expanded = "is_expanded";

    children.forEach((function(e) {
        var firstChild = e.firstChild;
        var hover = function() {
            reset();
            e.classList.add(active_sub_class);
            firstChild.classList.add(active_class)
        };
        var click = function(t) {
            t.preventDefault();
            firstChild.classList.contains(active_class) || reset();
            firstChild.classList.toggle(active_class);
            e.classList.toggle(active_sub_class)
        };
        e.addEventListener("mouseenter", hover);
        e.addEventListener("mouseleave", groupReset);
        e.addEventListener("focus", groupReset);
        firstChild.addEventListener("mousedown", click);
        firstChild.addEventListener("touchend", click);

    }));

    var setup = function() {
        children.forEach((function(e) {
                var active_sub = function() {
                    groupReset();
                    e.classList.add(active_sub_class)
                };
                e.addEventListener("keydown", (function(t) {
                        var r = e.firstChild.nextElementSibling;
                        if (13 === t.keyCode && t.target === e) {
                            t.preventDefault();
                            active_sub();
                            r.children[0].children[0].focus()
                        }
                        if (9 === t.keyCode) {
                            if (!e.classList.contains(active_sub_class))
                                return;
                            var findActive = document.activeElement.parentElement.nextElementSibling || document.activeElement.offsetParent.classList.contains("submenu") || null;
                            null === findActive && groupReset()
                        }
                        if (27 === t.keyCode) {
                            groupReset();
                            e.focus()
                        }
                    }
                ))
            }
        ));

        var e = nav.querySelectorAll("ul > li > a");
        e.forEach((function(e) {
                e.addEventListener("keydown", (function(n) {
                        var t = e.parentElement;
                        if (40 === n.keyCode) {
                            var r;
                            var a = (null === (r = t.nextSibling) || void 0 === r ? void 0 : r.children[0]) || null;
                            n.preventDefault();
                            null !== a && a.focus()
                        }
                        if (38 === n.keyCode) {
                            var o = t.previousSibling || null;
                            n.preventDefault();
                            null !== o && o.children[0].focus()
                        }
                        if (37 === n.keyCode) {
                            n.preventDefault();
                            e.parentElement.parentElement.previousSibling.focus()
                        }
                        return false
                    }
                ))
            }
        ));
        o.addEventListener("click", (function(e) {
                var n = nav.contains(e.target) || s.contains(e.target);
                n || groupReset()
            }
        ))
    };
    var setupToggleClass = function() {
        if (0 === f.length)
            return;
        f.forEach((function(e) {
                e.addEventListener("click", (function() {
                        e.classList.toggle("v--show")
                    }
                ));
                e.addEventListener("keydown", (function(n) {
                        if (13 === n.keyCode) {
                            n.preventDefault();
                            e.classList.toggle("v--show")
                        }
                    }
                ))
            }
        ))
    };

    function reset() {
        children.forEach((function(e) {
                e.classList.remove(active_sub_class);
                null !== e.querySelector("." + active_class) && e.querySelector("." + active_class).classList.remove(active_class)
            }
        ));
    }
    function submenuReset() {
        submenu.forEach((function(e) {
            e.classList.remove(expanded)
        }));
    }
    function sub_submenuReset() {
        sub_submenu.forEach((function(e) {
            e.classList.remove(expanded)
        }));
    }

    function groupReset() {
        reset();
        submenuReset();
        sub_submenuReset()
    }

    document.addEventListener("DOMContentLoaded", (function() {
        setup();
        setupToggleClass();
    }));



    /**
     * Mobile Toggle Button for Nav
     */
    $('.mobile-toggle').click(function() {
        $('body').toggleClass('overflow-y-hidden');
        $('header').toggleClass('header-active');
        $('header nav').toggleClass('nav-active');
    });


    /**
     * Mobile Toggle Button for Footer Navs
     */
    $('.js-footer-toggle').click(function() {
        $(this).find('button span').toggleClass('rotate-180');
        $(this).next().toggleClass('hidden');
    });



    /**
     * custom swiper
     */
    if($('.js-custom-swiper').length) {

        var custom_swiper = new Swiper('.js-custom-swiper', {
            pagination: {
                el: '.swiper-pagination',
                type: 'bullets',
                clickable : false,
                dynamicBullets : true,
            },
            navigation: {
                nextEl: '.js-custom-swiper-next',
                prevEl: '.js-custom-swiper-prev',
            },
            slidesPerView: 4,
            spaceBetween: 32,
            breakpoints: {
                1280: {
                    slidesPerView: 3,
                    spaceBetween: 16
                },
                992: {
                    slidesPerView: 3,
                    spaceBetween: 16
                },
                768: {
                    slidesPerView: 2,
                    spaceBetween: 16
                },
                480: {
                    slidesPerView: 1,
                    spaceBetween: 16
                }
            },
            preloadImages : false,
            autoplay: {
                delay: 5000,
                disableOnInteraction: true,
            },
        });

        $(".js-custom-swiper").hover(function() {
            custom_swiper.autoplay.stop();
        }, function() {
            custom_swiper.autoplay.start();
        });
    }



    /**
     * Handle Load More Pagination
     */
    var next = jQuery('.js-pagination').find('.next');
    if(next.length == 0){
        jQuery('.js-load-more').addClass('hidden');
    }

    jQuery('.js-load-more').click(function(){

        var next = jQuery('.js-pagination').find('.next');

        jQuery.post( next.attr('href'), function( data ) {
            var content = jQuery(data).find('.js-results').html();
            var pagination = jQuery(data).find('.js-pagination');
            jQuery('.js-pagination').html(pagination);
            jQuery('.js-results').append(content);
           
            var checkNext = jQuery(pagination).find('.next');
            if(checkNext.length == 0){
                jQuery('.js-load-more').addClass('hidden');
            }

        });

    });

    /**
     * Object Fit Fix for IE
     */
    if ('objectFit' in document.documentElement.style === false) {
        var wrap = $('.lazyload');
        wrap.each(function () {
            var $container = $(this),
                imgUrl = $container.find('img').attr('src');
            if (imgUrl) {
                $container
                    .css('backgroundImage', 'url(' + imgUrl + ')')
                    .addClass('compat-object-fit');
            }
        });
    }

});