window.addEventListener("load", function () {
    window.cookieconsent.initialise({
        palette: {
            popup: {background: "rgba(255,255,255,0.9)", text: "#000"},
            button: {background: "#000", text: "#fff"},
        },
        content: {
            message: "This website uses cookies to make sure you get the best experience. By clicking “Accept All Cookies,” you agree to the storing of first and third-party cookies.",
            dismiss: "Accept all cookies",
            link: "Cookie Policy",
            target: '_blank',
            href: "/cookie-policy/",
        },
        revokable: true,
        onRevokeChoice: function (status) {
            // This does the ajax request
            var cookies = document.cookie.split(";");

            for (var i = 0; i < cookies.length; i++) {
                var cookie = cookies[i];
                var eqPos = cookie.indexOf("=");
                var name = eqPos > -1 ? cookie.substr(0, eqPos) : cookie;
                document.cookie = name + "=;expires=Thu, 01 Jan 1970 00:00:00 GMT";
            }

            document.location.reload(true);
        },
        onStatusChange: function (status, chosenBefore) {
            document.location.reload(true);
        },
        law: {
            regionalLaw: false,
        },
        location: true,
    });

    jQuery('.revoke').on('click', function () {
        if (window.confirm("Are you sure you want to disable cookies?")) {

            document.cookie.split(";").forEach(function (c) {
                document.cookie = c.replace(/^ +/, "").replace(/=.*/, "=;expires=" + new Date().toUTCString() + ";path=/");
            });

            location.reload();
        }
    });
});
