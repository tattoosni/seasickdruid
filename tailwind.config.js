const colors = require('tailwindcss/colors')

module.exports = {
  mode: 'jit',
  purge: [
    '*.php',
    './assets/js/*.js',
    './assets/blocks/*.php',
    './assets/parts/*/*.php',
    './assets/components/*.php',
    './assets/functions/*.php',
    './templates/*.php',
    './woocommerce/*.php',
    './woocommerce/*/*.php'
  ],
  darkMode: false,
  theme: {
    container: {
      center: true,
      padding:{
        DEFAULT: '1rem',
        sm: '2rem',
        lg: '2rem',
        xl: '2rem',
        '2xl': '2rem',
      },
    },
    colors: {
      red: '#A31616',
      dkred: '#6E2F2F',
      white: '#fff',
      ltgray: '#C6C6C6',
      dkgray: '#0A0A0A',
      black: '#000',
      transparent: 'transparent',
      gray: colors.coolGray,
    },
    fontFamily: {
      heading: ['Nunito', 'sans-serif'],
      body: ['Nunito', 'sans-serif'],
    },
    fontSize: {
      xs: '0.75rem',//12
      sm: '0.875rem',//14
      base: '1rem',//16
      'h6': '0.875rem',//14
      'h5': '1rem',//16
      'h4': '1.125rem',//18
      'h3': '1.75rem',//28
      'h2': '2.375rem',//38
      'h1': '3rem',//48
      'hero': '3.625rem',//58
    },
    extend: {
       spacing: {
         fphero: '600px',
         pghero: '350px',
         minussix: '-1rem',
      //   '128': '32rem',
      //   '144': '36rem',
       },
    }
  },
  variants: {
    extend: {
      display:['group-hover'],
    }
  },
  plugins: [
    require('@tailwindcss/aspect-ratio'),
    require('@tailwindcss/forms'),
  ]
}