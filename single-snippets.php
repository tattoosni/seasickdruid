<?php get_header(); ?>

    <div class="absolute w-full top-0 left-0 right-0 bg-gray-200 z-[-1] h-[50vh]">
        <?php WP_Image::display(array('class' => 'object-cover h-[50vh] w-full')); ?>
    </div>

    <section class="mt-[400px] min-h-[600px]">
        <div class="container">
            <article class="relative">
                <div class="lg:w-1/3 w-full lg:absolute top-[-3%] left-0 z-10 mb-4 bg-grey-200">
                    <?php WP_Image::display(array('class' => 'w-full object-cover border-2 border-white shadow-lg rounded-xl ')); ?>
                </div>
                <div class="flex py-4 mt-24">
                    <div class="w-1/3 lg:block hidden"></div>
                    <div class="lg:w-2/3 w-full border bg-white rounded-xl shadow-lg lg:ml-minussix m-0 px-6">
                        <h1 class="text-center break-words text-red uppercase my-12"><?php the_title(); ?></h1>
                        <?php the_content(); ?>
                    </div>
                </div>
            </article>
        </div>
    </section>

<?php get_footer(); ?>
