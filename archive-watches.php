<?php get_header(); ?>

<section class="my-32">
    <div class="p-4">
        <div class="row js-results">
            <?php while (have_posts()) : the_post(); ?>

                <div class="w-full md:w-1/4 md:mb-12 mb-4">
                    <?php get_template_part('assets/parts/cards/card-default'); ?>
                </div>

            <?php endwhile;?>
        </div>
        <div class="text-center my-12">
            <?php pagination(); ?>
        </div>
    </div>
</section>

<?php get_footer(); ?>