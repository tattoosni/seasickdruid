<?php
///////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////
/////////       ///////////////////////////////////////////
/////////         //////////////////.       ///////////////
/////////          /////////////////  ////   //////////////
/////////          //////        ///  ////   //////////////
/////////          ///////        ///   //   /////// */////
/////////          ///////         ///// .   ////*  ///////
/////////          //////.         //////    ,..,//////////
/////////          ///// .         ./////   ///////////////
//////////         ////             ////    ///////////////
//////////          //   //         ///    ////////////////
//////////              ///               /////////////////
///////////           //////             //////////////////
/////////////     ,//////////          ////////////////////
//////////////////////////////      ///////////////////////
///////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////
////////////////////////////////////////////////wearewibble


// For use in <head> to set global site colour in Google Mobile Browsers
define('THEME_COLOUR', '#123456'); //


//Google Analytics Tracking ID
define('G_TAG', false); //format - UA-123456-1


//Theme Support for different features
add_theme_support('title-tag');
add_theme_support('post-thumbnails');
add_theme_support('align-wide');
add_theme_support('disable-custom-colors');
//add_theme_support( 'woocommerce' );
add_theme_support('editor-color-palette', array(
    array(
        'name' => 'White',
        'slug' => 'white',
        'color' => '#f5f5f5',
    ),
    array(
        'name' => 'Black',
        'slug' => 'black',
        'color' => '#000000',
    )
));


require_once( __DIR__ . '/assets/functions/fn-enqueue.php');
require_once( __DIR__ . '/assets/functions/fn-cpt.php');
require_once( __DIR__ . '/assets/functions/fn-nav.php');
require_once( __DIR__ . '/assets/functions/fn-image.php');


if( class_exists('ACF') ){
    require_once( __DIR__ . '/assets/functions/fn-acf.php');
}


if ( class_exists( 'GFCommon' ) ) {
    require_once( __DIR__ . '/assets/functions/fn-gforms.php');
}


if (class_exists('WPSEO_Options')) {
    function yoasttobottom()
    {
        return 'low';
    }

    add_filter('wpseo_metabox_prio', 'yoasttobottom');
}


//Hide some pages from admin.
function remove_from_admin_menu() {
    //remove_menu_page('edit.php');
}
add_action('admin_menu', 'remove_from_admin_menu');


//change excerpt settings
//add_filter( 'excerpt_more', fn() => '...' );
//add_filter( 'excerpt_length', fn() => 40 );
//better excerpt function
function excerpt($length = 20, $after = '...') {
    echo wp_trim_words(get_the_excerpt(), $length, $after);
}
